// Package player encapsulates information about player and
// provides functions for player data manipulation.
package player

import (
	"log"
	"math/rand"
	"sort"
	"strconv"

	"gitlab.com/alexbenic/tourny/pkg/json"
)

// Player represents a player.
type Player struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Points    string `json:"points"`
	Seed      int    `json:"-"`
}

// Players ...
type Players []Player

// Len - Sort interface
func (ps Players) Len() int {
	return len(p)
}

// Less - Sort interface
func (ps Players) Less(i, j int) bool {
	f, err := strconv.Atoi(ps[i].Points)
	if err != nil {
		log.Fatal(json.ErrPointsFormat)
	}
	s, err := strconv.Atoi(ps[j].Points)
	if err != nil {
		log.Fatal(json.ErrPointsFormat)
	}
	return f > s
}

// Swap - Sort interface
func (ps Players) Swap(i, j int) {
	ps[i], ps[j] = ps[j], ps[i]
}

// Fragment recursively breaks array in two
// based on current depth. First iteration is
// essentially double shift operation.
// Ex.
// arr := [3, 4, 5, 6, 7, 8]  iter=0
// a = 3, b = 4
// arr := [5, 6, 7, 8]		iter=1
// x = [5, 6], y = [7, 8]
// Then we assemble them in "random" order. We
// "flip a coin" ( rand.Float32() < 0.5 ) and do
// append(a, x) && append(b, y), or the other way
// around.
// We have `depth - 1` random assemblies.
func (ps *Players) Fragment(depth int) (Players, Players) {
	level := 1 << depth
	a := make(Players, len(ps[:level]))
	b := make(Players, len(ps[:level]))
	_, ps = copy(a, ps[:level]), ps[level:]
	_, ps = copy(b, ps[:level]), ps[level:]

	if len(ps) == 0 {
		return a, b
	}

	x, y := ps.Fragment(depth + 1)

	if rand.Float32() > 0.5 {
		return append(a, x...), append(b, y...)
	}

	return append(a, y...), append(b, x...)
}

// Sort sorts players. In functional programming this is
// called Thrush combinator.
func (ps *Players) Sort() Players {
	return sort.Sort(ps)
}
