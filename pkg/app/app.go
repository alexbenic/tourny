// Package app is main point of interaction with this domain.
package app

import (
	"log"

	"gitlab.com/alexbenic/tourny/pkg/json"
	"gitlab.com/alexbenic/tourny/pkg/player"
	"gitlab.com/alexbenic/tourny/pkg/tree"
)

// Printer ...
type Printer interface {
	PrintLadder(ps player.Players) string
	PrintPredictions(ps player.Players) string
}

// App ...
type App struct {
	ps   []Players
	tree *Tree
}

// New ...
func New(path string) *App {
	app := &App{}

	// read from json, we do this only once
	// on start-up
	ps, err := json.ReadFrom(path)
	if err != nil {
		log.Fatal(err)
	}

	// sort it
	ps.Sort()
	// and add seed value
	for i := range ps {
		ps[i].Seed = i + 1
	}

	// attach it to app.ps
	app.ps = ps

	// generate a tree representation
	// ?maybe do this concurrently in
	// the future, app should hold sync.WaitGroup
	// ref in that case
	tree, err := tree.New(ps)
	if err != nil {
		log.Fatal(err)
	}

	app.tree = tree

	return app
}

// Generate re-generates a tree
func (a *App) Generate() error {
	tree, err := tree.New(a.ps)
	if err != nil {
		return err
	}

	// switch trees
	a.tree = tree
	return nil
}

// Search looks up possible opponents for player with
// `name` in the n-th `round`.
func (a *App) Search(name string, round int) error {
	a.tree.Search(name, round)
}
