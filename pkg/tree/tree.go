// Package tree is main skeleton represenation. Give the sorted slice
// (decending order) it generates new tree recursively top-down. It starts
// from root node by shifting first two values ( first and second seed )
// and recurevly calls it self.
//					{1,2}
//					/	\
//				{1,_}	{2,_}
//				/	\   /	\
//			{1,_}{_,_}{2,_}{_,_}
//	On each pass it fragments the remaining array, and "flips the coin"
//	(expression that evaluates to true half of the time `rand.Float32() < 0.5`)
//	so, given tree above it might generate tree like this:
//					{1,2}
//					/	\
//				{1,3}	{2,4}
//				/	\   /	\
//			{1,_}{_,_}{2,_}{_,_}
//	  `fragment remaining players and randomize`
//
//					{1,2}
//					/	\
//				{1,3}	{2,4}
//				/	\   /	\
//			{1,8}{3,7}{2,5}{4,6}
package tree

import (
	"errors"
	"html/template"
	"net/http"

	"gitlab.com/alexbenic/tourny/pkg/player"
	"gitlab.com/alexbenic/tourny/pkg/web"
)

const (
	// ErrEmptyPlayers is emitted when empty slice of Players is passed to tree constructor.
	ErrEmptyPlayers = errors.New("Cannot construct tree with empty slice of Players")

	// ErrNumOfPlayers is emitted emitted when len of Players is not power of two.
	ErrNumOfPlayers = errors.New("Number of participants should be power of 2; ex. 8, 16, 32, 64...")
)

// Direction marks a subtree. It is `a path taken` while doing second
// part of the search
type Direction int

const (
	// Left ...
	Left Direction = iota
	// Right ...
	Right
)

// Order of traversing tree
type Order int

const (
	// Pre is a tree traversal option
	Pre Order = iota
	// In order is a tree traversal option
	In
	// Post order is a tree traversal option
	Post
)

// Tree represents tournament skeleton.
type Tree struct {
	Root *Node
}

// Node is struct that holds data about match
type Node struct {
	Value Match
	Left  *Node
	Right *Node
}

// Match is convenient struct that represent main
// value that Node represents.
type Match struct {
	First  player.Player
	Second player.Player
}

// info is struct used to keep reference to `round` node.
// It is used in Search functionality of the tree.
// It is passed to recursive function as reference on which
// node to call print function in case the player is found in
// leaf node.
type info struct {
	node *Node
	lr   Direction
}

// New generates new tree. Given the sorted slice of players
// it generates random tournament skeleton
func New(ps player.Players) (*Tree, error) {
	var t *Tree
	if len(ps) == 0 {
		return t, errors.New("Cannot construct tree with empty slice of Players")
	} else if len(ps) & (len(ps) - 1) { // is power of two?  1000 & 0111 == 0?
		return t, errors.New("Number of participants should be power of 2; ex. 8, 16, 32, 64...")
	}

	t = &Tree{}
	t.Root = &Node{}
	t.Root.generate(ps)
	return t, nil
}

// Traverse the tree in postfix order start with n Node. It accepts order of traversal
// and a callback which is used to inspect state.
func (t *Tree) Traverse(n *Node, o Order, f func(*Node, int), depth int) {
	if n == nil {
		return
	}
	if o == Pre {
		f(n, depth)
		t.Traverse(n.Left, f, depth+1)
		t.Traverse(n.Right, f, depth+1)
	} else if o == In {
		t.Traverse(n.Left, f, depth+1)
		f(n, depth)
		t.Traverse(n.Right, f, depth+1)
	}
	t.Traverse(n.Left, f, depth+1)
	t.Traverse(n.Right, f, depth+1)
	f(n, depth)
}

// generate builds the tree. It recursively calls its self
// until it depletes slice of players
func (t *Node) generate(ps player.Players) {

	// shift twice
	f, ps := ps[0], ps[1:]
	s, ps := ps[0], ps[1:]

	t.Value = Match{First: f, Second: s}

	if len(ps) == 0 {
		return
	}

	x, y := ps.Fragment(0)

	a := make(player.Players, len(x))
	b := make(player.Players, len(x))
	_, _ = copy(a, x), copy(b, y)
	// we could add another level of randomness here
	a, b = append(Players{f}, a...), append(Players{s}, b...)

	t.Left = &Node{}
	t.Right = &Node{}

	t.Left.generate(a)
	t.Right.generate(b)
}

// Search is essentially a task of finding the right subtree, walking
// it and printing results in desired format.
// First we traverse the tree looking for right `round` ( 1/16, 1/8 ),
// we are looking for right depth.
// When we find it we call t.search(...) which has one extra argument,
// reference to the current node `n *Node` and path we took `Left | Right`
// If we find player that matches `name` in one of the leaf nodes we call
// `printTree` function on ref node on opposite side.
// Ex.
// We found player in leaf node, `ref node` is info{n, Right}, we then call
// `print(n.Left)`, thus printing a subtree
func (t *Tree) Search(name string, depth int, w http.ResponseWriter) {
	fn := func(n *Node, d int) {
		if depth == d {
			search(n.Left, name, depth, info{node: n, lr: Left}, w)
			search(n.Right, name, depth, info{node: n, lr: Right}, w)
		}
	}
	t.Traverse(t.Root, Pre, fn, 0)
}

func search(n *Node, name string, depth int, m info, w http.ResponseWriter) {
	if n == nil {
		return
	}
	// adjust this depth; needs to be max depth - subtree root depth
	if depth == 3 {
		first := n.Value.First.FirstName + " " + n.Value.First.LastName
		second := n.Value.Second.FirstName + " " + n.Value.Second.LastName
		if first == name || second == name {
			if m.lr == Left {
				printSubTree(m.Node.Right, Right, w)
				return
			}
			printSubTree(m.Node.Left, Left, w)
			return
		}
	}
	search(n.Left, name, depth+1, m, w)
	search(n.Right, name, depth+1, m, w)
	return
}

func printSubTree(n *Node, d Direction, w http.ResponseWriter) {
	m := make(map[string]Player, 4)
	if d == Left {
		straverse(n, m)
		printTable(m, w)
		// for k := range m {
		// 	log.Printf("%s\n", k)
		// }
		return
	}
	straverse(n, m)
	printTable(m, w)
	// for k := range m {
	// 	log.Printf("%s\n", k)
	// }
}

func printTable(m map[string]Player, w http.ResponseWriter) {
	tmpl := template.New("table")
	tmpl, err := tmpl.Parse(web.Table)
	if err != nil {
		http.Error(w, "Whoops", http.StatusInternalServerError)
	}

	_ = tmpl.Execute(w, struct {
		Players map[string]Player
	}{
		Players: m,
	})
}

func straverse(n *Node, m map[string]Player) {
	if n == nil {
		return
	}

	first := n.Value.First.FirstName + " " + n.Value.First.LastName
	second := n.Value.Second.FirstName + " " + n.Value.Second.LastName

	m[first] = n.Value.First
	m[second] = n.Value.Second
	straverse(n.Left, m)
	straverse(n.Right, m)

}
