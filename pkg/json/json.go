package json

import (
	"encoding/json"
	"errors"
	"io/ioutil"

	"gitlab.com/alexbenic/tourny/pkg"
)

const (
	// ErrBadFormat is thrown when passed json is not well formatted
	ErrBadFormat = errors.New("json: bad formatted json")
	// ErrPointsFormat is thrown when points is not a number
	ErrPointsFormat = errors.New("json: unable to parse points")
)

// ReadFrom is a utility function that reads and unmarshals
// json from given file
func ReadFrom(file string) (pkg.Players, error) {
	var ps pkg.Players

	d, err := ioutil.ReadFile(file)
	if err != nil {
		return ps, err
	}

	err = json.Unmarshal(d, &ps)
	return ps, nil
}
