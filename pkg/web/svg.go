package web

// Svg is inline template for tournament sceleton
const Svg = `<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 1000 720" style="enable-background:new 0 0 1000 720;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#1A1A1A;}
	.st1{fill:#4D4D4D;}
	.st2{fill:#CCCCCC;}
	.st3{fill:none;}
	.st4{font-family:'Helvetica';}
	.st5{font-size:8px;}
	.st6{stroke:#000000;stroke-miterlimit:10;}
	.st7{fill:none;stroke:#000000;stroke-miterlimit:10;}
	.st8{fill:none;stroke:#000000;stroke-width:2;stroke-miterlimit:10;}
	.st9{fill:none;stroke:#000000;stroke-width:3;stroke-miterlimit:10;}
	.st10{fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}
</style>
<g id="Layer_1">
	<g id="XMLID_1_">
		<rect id="XMLID_5_" x="58" y="30.7" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_6_" x="58" y="46.3" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_7_" x="137" y="30.7" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_8_" x="58" y="46" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_9_" x="136.8" y="30.7" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_584_" x="58" y="34.5" class="st3" width="78.8" height="8.1"/>
		{{with index .Matches 0}}
		<text id="XMLID_11_" transform="matrix(1 0 0 1 60.3442 42.1098)" class="st2 st4 st5">{{.First.FirstName}} {{.First.LastName}}</text>
		<rect id="XMLID_581_" x="58" y="50" class="st3" width="78.8" height="8.1"/>
		<text id="XMLID_14_" transform="matrix(1 0 0 1 60.3442 57.6606)" class="st2 st4 st5">{{.Second.FirstName}} {{.Second.LastName}}</text>
		<rect id="XMLID_578_" x="137.3" y="34.5" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_20_" transform="matrix(1 0 0 1 140.0205 42.1098)" class="st4 st5">{{.First.Seed}}</text>
		<rect id="XMLID_575_" x="137.3" y="50" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_17_" transform="matrix(1 0 0 1 140.0205 57.6606)" class="st2 st4 st5">{{.Second.Seed}}</text>
		{{end}}
	</g>
	<g id="XMLID_2_">
		<rect id="XMLID_36_" x="58" y="74.8" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_35_" x="58" y="90.3" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_34_" x="137" y="74.8" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_33_" x="58" y="90" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_32_" x="136.8" y="74.8" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_566_" x="58" y="78.5" class="st3" width="78.8" height="8.1"/>
		{{with index .Matches 1}}
		<text id="XMLID_30_" transform="matrix(1 0 0 1 60.3442 86.1311)" class="st2 st4 st5">{{.First.FirstName}} {{.First.LastName}}</text>
		<rect id="XMLID_563_" x="58" y="94" class="st3" width="78.8" height="8.1"/>
		<text id="XMLID_27_" transform="matrix(1 0 0 1 60.3442 101.6824)" class="st2 st4 st5">{{.Second.FirstName}} {{.Second.LastName}}</text>
		<rect id="XMLID_560_" x="137.3" y="78.5" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_24_" transform="matrix(1 0 0 1 140.02 86.1311)" class="st4 st5">{{.First.Seed}}</text>
		<rect id="XMLID_557_" x="137.3" y="94" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_4_" transform="matrix(1 0 0 1 140.02 101.6824)" class="st2 st4 st5">{{.Second.Seed}}</text>
		{{end}}
	</g>
	<g id="XMLID_41_">
		<rect id="XMLID_58_" x="58" y="115.8" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_57_" x="58" y="131.3" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_56_" x="137" y="115.8" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_55_" x="58" y="131" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_54_" x="136.8" y="115.8" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_548_" x="58" y="119.5" class="st3" width="78.8" height="8.1"/>
		{{with index .Matches 2}}
		<text id="XMLID_52_" transform="matrix(1 0 0 1 60.3442 127.1311)" class="st2 st4 st5">{{.First.FirstName}} {{.First.LastName}}</text>
		<rect id="XMLID_545_" x="58" y="135" class="st3" width="78.8" height="8.1"/>
		<text id="XMLID_49_" transform="matrix(1 0 0 1 60.3442 142.6824)" class="st2 st4 st5">{{.Second.FirstName}} {{.Second.LastName}}</text>
		<rect id="XMLID_542_" x="137.3" y="119.5" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_46_" transform="matrix(1 0 0 1 140.02 127.1311)" class="st4 st5">{{.First.Seed}}</text>
		<rect id="XMLID_539_" x="137.3" y="135" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_43_" transform="matrix(1 0 0 1 140.02 142.6824)" class="st2 st4 st5">{{.Second.Seed}}</text>
		{{end}}
	</g>
	<g id="XMLID_69_">
		<rect id="XMLID_86_" x="58" y="157.8" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_85_" x="58" y="173.3" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_84_" x="137" y="157.8" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_83_" x="58" y="173" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_82_" x="136.8" y="157.8" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_530_" x="58" y="161.5" class="st3" width="78.8" height="8.1"/>
		{{ with index .Matches 3 }}
		<text id="XMLID_80_" transform="matrix(1 0 0 1 60.3442 169.1311)" class="st2 st4 st5">{{.First.FirstName}} {{.First.LastName}}</text>
		<rect id="XMLID_527_" x="58" y="177" class="st3" width="78.8" height="8.1"/>
		<text id="XMLID_77_" transform="matrix(1 0 0 1 60.3442 184.6824)" class="st2 st4 st5">{{.Second.FirstName}} {{.Second.LastName}}</text>
		<rect id="XMLID_524_" x="137.3" y="161.5" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_74_" transform="matrix(1 0 0 1 140.02 169.1311)" class="st4 st5">{{.First.Seed}}</text>
		<rect id="XMLID_520_" x="137.3" y="177" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_71_" transform="matrix(1 0 0 1 140.02 184.6824)" class="st2 st4 st5">{{.Second.Seed}}</text>
		{{ end }}
	</g>
	<g id="XMLID_97_">
		<rect id="XMLID_114_" x="58" y="199.8" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_113_" x="58" y="215.3" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_112_" x="137" y="199.8" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_111_" x="58" y="215" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_110_" x="136.8" y="199.8" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_508_" x="58" y="203.5" class="st3" width="78.8" height="8.1"/>
		{{with index .Matches 4}}
		<text id="XMLID_108_" transform="matrix(1 0 0 1 60.3442 211.1311)" class="st2 st4 st5">{{.First.FirstName}} {{.First.LastName}}</text>
		<rect id="XMLID_505_" x="58" y="219" class="st3" width="78.8" height="8.1"/>
		<text id="XMLID_105_" transform="matrix(1 0 0 1 60.3442 226.6824)" class="st2 st4 st5">{{.Second.FirstName}} {{.Second.LastName}}</text>
		<rect id="XMLID_501_" x="137.3" y="203.5" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_102_" transform="matrix(1 0 0 1 140.02 211.1311)" class="st4 st5">{{.First.Seed}}</text>
		<rect id="XMLID_496_" x="137.3" y="219" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_99_" transform="matrix(1 0 0 1 140.02 226.6824)" class="st2 st4 st5">{{.Second.Seed}}</text>
		{{end}}
	</g>
	<g id="XMLID_211_">
		<rect id="XMLID_228_" x="58" y="241.8" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_227_" x="58" y="257.3" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_226_" x="137" y="241.8" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_225_" x="58" y="257" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_224_" x="136.8" y="241.8" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_486_" x="58" y="245.5" class="st3" width="78.8" height="8.1"/>
		{{ with index .Matches 5 }}
		<text id="XMLID_222_" transform="matrix(1 0 0 1 60.3442 253.1311)" class="st2 st4 st5">{{.First.FirstName}} {{.First.LastName}}</text>
		<rect id="XMLID_481_" x="58" y="261" class="st3" width="78.8" height="8.1"/>
		<text id="XMLID_219_" transform="matrix(1 0 0 1 60.3442 268.6824)" class="st2 st4 st5">{{.Second.FirstName}} {{.Second.LastName}}</text>
		<rect id="XMLID_477_" x="137.3" y="245.5" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_216_" transform="matrix(1 0 0 1 140.02 253.1311)" class="st4 st5">{{.First.Seed}}</text>
		<rect id="XMLID_473_" x="137.3" y="261" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_213_" transform="matrix(1 0 0 1 140.02 268.6824)" class="st2 st4 st5">{{.Second.Seed}}</text>
		{{end}}
	</g>
	<g id="XMLID_286_">
		<rect id="XMLID_303_" x="58" y="285.8" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_302_" x="58" y="301.3" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_301_" x="137" y="285.8" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_300_" x="58" y="301" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_299_" x="136.8" y="285.8" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_462_" x="58" y="289.5" class="st3" width="78.8" height="8.1"/>
		{{ with index .Matches 6 }}
		<text id="XMLID_297_" transform="matrix(1 0 0 1 60.3442 297.1529)" class="st2 st4 st5">{{.First.FirstName}} {{.First.LastName}}</text>
		<rect id="XMLID_457_" x="58" y="305.1" class="st3" width="78.8" height="8.1"/>
		<text id="XMLID_294_" transform="matrix(1 0 0 1 60.3442 312.7042)" class="st2 st4 st5">{{.Second.FirstName}} {{.Second.LastName}}</text>
		<rect id="XMLID_454_" x="137.3" y="289.5" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_291_" transform="matrix(1 0 0 1 140.02 297.1529)" class="st4 st5">{{.First.Seed}}</text>
		<rect id="XMLID_451_" x="137.3" y="305.1" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_288_" transform="matrix(1 0 0 1 140.02 312.7042)" class="st2 st4 st5">{{.Second.Seed}}</text>
		{{end}}
	</g>
	<g id="XMLID_173_">
		<rect id="XMLID_190_" x="58" y="326.8" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_189_" x="58" y="342.3" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_188_" x="137" y="326.8" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_187_" x="58" y="342" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_186_" x="136.8" y="326.8" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_438_" x="58" y="330.5" class="st3" width="78.8" height="8.1"/>
		{{ with index .Matches 7 }}
		<text id="XMLID_184_" transform="matrix(1 0 0 1 60.3442 338.1529)" class="st2 st4 st5">{{.First.FirstName}} {{.First.LastName}}</text>
		<rect id="XMLID_435_" x="58" y="346.1" class="st3" width="78.8" height="8.1"/>
		<text id="XMLID_181_" transform="matrix(1 0 0 1 60.3442 353.7042)" class="st2 st4 st5">{{.Second.FirstName}} {{.Second.LastName}}</text>
		<rect id="XMLID_432_" x="137.3" y="330.5" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_178_" transform="matrix(1 0 0 1 140.02 338.1529)" class="st4 st5">{{.First.Seed}}</text>
		<rect id="XMLID_427_" x="137.3" y="346.1" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_175_" transform="matrix(1 0 0 1 140.02 353.7042)" class="st2 st4 st5">{{.Second.Seed}}</text>
		{{ end }}
	</g>
	<g id="XMLID_250_">
		<rect id="XMLID_267_" x="58" y="368.8" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_266_" x="58" y="384.3" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_265_" x="137" y="368.8" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_264_" x="58" y="384" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_263_" x="136.8" y="368.8" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_416_" x="58" y="372.5" class="st3" width="78.8" height="8.1"/>
		{{ with index .Matches 8 }}
		<text id="XMLID_261_" transform="matrix(1 0 0 1 60.3442 380.1529)" class="st2 st4 st5">{{.First.FirstName}} {{.First.LastName}}</text>
		<rect id="XMLID_412_" x="58" y="388.1" class="st3" width="78.8" height="8.1"/>
		<text id="XMLID_258_" transform="matrix(1 0 0 1 60.3442 395.7042)" class="st2 st4 st5">{{.Second.FirstName}} {{.Second.LastName}}</text>
		<rect id="XMLID_408_" x="137.3" y="372.5" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_255_" transform="matrix(1 0 0 1 140.02 380.1529)" class="st4 st5">{{.First.Seed}}</text>
		<rect id="XMLID_403_" x="137.3" y="388.1" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_252_" transform="matrix(1 0 0 1 140.02 395.7042)" class="st2 st4 st5">{{.Second.Seed}}</text>
		{{end}}
	</g>
	<g id="XMLID_229_">
		<rect id="XMLID_253_" x="58" y="412" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_249_" x="58" y="427.6" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_248_" x="137" y="412" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_247_" x="58" y="427.3" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_246_" x="136.8" y="412" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_393_" x="58" y="415.8" class="st3" width="78.8" height="8.1"/>
		{{ with index .Matches 9 }}
		<text id="XMLID_244_" transform="matrix(1 0 0 1 60.3442 423.4231)" class="st2 st4 st5">{{.First.FirstName}} {{.First.LastName}}</text>
		<rect id="XMLID_388_" x="58" y="431.3" class="st3" width="78.8" height="8.1"/>
		<text id="XMLID_240_" transform="matrix(1 0 0 1 60.3442 438.9744)" class="st2 st4 st5">{{.Second.FirstName}} {{.Second.LastName}}</text>
		<rect id="XMLID_384_" x="137.3" y="415.8" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_235_" transform="matrix(1 0 0 1 140.02 423.4231)" class="st4 st5">{{.First.Seed}}</text>
		<rect id="XMLID_381_" x="137.3" y="431.3" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_231_" transform="matrix(1 0 0 1 140.02 438.9744)" class="st2 st4 st5">{{.Second.Seed}}</text>
		{{ end }}
	</g>
	<g id="XMLID_193_">
		<rect id="XMLID_223_" x="58" y="454" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_220_" x="58" y="469.6" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_217_" x="137" y="454" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_214_" x="58" y="469.3" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_210_" x="136.8" y="454" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_369_" x="58" y="457.8" class="st3" width="78.8" height="8.1"/>
		{{ with index .Matches 10 }}
		<text id="XMLID_208_" transform="matrix(1 0 0 1 60.3442 465.4231)" class="st2 st4 st5">{{.First.FirstName}} {{.First.LastName}}</text>
		<rect id="XMLID_365_" x="58" y="473.3" class="st3" width="78.8" height="8.1"/>
		<text id="XMLID_204_" transform="matrix(1 0 0 1 60.3442 480.9744)" class="st2 st4 st5">{{.Second.FirstName}} {{.Second.LastName}}</text>
		<rect id="XMLID_362_" x="137.3" y="457.8" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_200_" transform="matrix(1 0 0 1 140.02 465.4231)" class="st4 st5">{{.First.Seed}}</text>
		<rect id="XMLID_358_" x="137.3" y="473.3" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_196_" transform="matrix(1 0 0 1 140.02 480.9744)" class="st2 st4 st5">{{.Second.Seed}}</text>
		{{end}}
	</g>
	<g id="XMLID_158_">
		<rect id="XMLID_192_" x="58" y="496" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_191_" x="58" y="511.6" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_185_" x="137" y="496" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_182_" x="58" y="511.3" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_179_" x="136.8" y="496" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_346_" x="58" y="499.8" class="st3" width="78.8" height="8.1"/>
		{{ with index .Matches 11 }}
		<text id="XMLID_172_" transform="matrix(1 0 0 1 60.3442 507.4231)" class="st2 st4 st5">{{.First.FirstName}} {{.First.LastName}}</text>
		<rect id="XMLID_343_" x="58" y="515.3" class="st3" width="78.8" height="8.1"/>
		<text id="XMLID_168_" transform="matrix(1 0 0 1 60.3442 522.9744)" class="st2 st4 st5">{{.Second.FirstName}} {{.Second.LastName}}</text>
		<rect id="XMLID_339_" x="137.3" y="499.8" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_164_" transform="matrix(1 0 0 1 140.02 507.4231)" class="st4 st5">{{.First.Seed}}</text>
		<rect id="XMLID_334_" x="137.3" y="515.3" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_160_" transform="matrix(1 0 0 1 140.02 522.9744)" class="st2 st4 st5">{{.Second.Seed}}</text>
		{{end}}
	</g>
	<g id="XMLID_136_">
		<rect id="XMLID_156_" x="58" y="538" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_155_" x="58" y="553.6" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_154_" x="137" y="538" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_153_" x="58" y="553.3" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_152_" x="136.8" y="538" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_324_" x="58" y="541.8" class="st3" width="78.8" height="8.1"/>
		{{ with index .Matches 12 }}
		<text id="XMLID_150_" transform="matrix(1 0 0 1 60.3442 549.4231)" class="st2 st4 st5">{{.First.FirstName}} {{.First.LastName}}</text>
		<rect id="XMLID_319_" x="58" y="557.3" class="st3" width="78.8" height="8.1"/>
		<text id="XMLID_147_" transform="matrix(1 0 0 1 60.3442 564.9744)" class="st2 st4 st5">{{.Second.FirstName}} {{.Second.LastName}}</text>
		<rect id="XMLID_315_" x="137.3" y="541.8" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_143_" transform="matrix(1 0 0 1 140.02 549.4231)" class="st4 st5">{{.First.Seed}}</text>
		<rect id="XMLID_311_" x="137.3" y="557.3" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_139_" transform="matrix(1 0 0 1 140.02 564.9744)" class="st2 st4 st5">{{.Second.Seed}}</text>
		{{end}}
	</g>
	<g id="XMLID_109_">
		<rect id="XMLID_134_" x="58" y="582.1" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_133_" x="58" y="597.6" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_132_" x="137" y="582.1" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_131_" x="58" y="597.3" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_130_" x="136.8" y="582.1" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_289_" x="58" y="585.8" class="st3" width="78.8" height="8.1"/>
		{{ with index .Matches 13 }}
		<text id="XMLID_128_" transform="matrix(1 0 0 1 60.3442 593.4449)" class="st2 st4 st5">{{.First.FirstName}} {{.First.LastName}}</text>
		<rect id="XMLID_281_" x="58" y="601.4" class="st3" width="78.8" height="8.1"/>
		<text id="XMLID_125_" transform="matrix(1 0 0 1 60.3442 608.9962)" class="st2 st4 st5">{{.Second.FirstName}} {{.Second.LastName}}</text>
		<rect id="XMLID_278_" x="137.3" y="585.8" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_121_" transform="matrix(1 0 0 1 140.02 593.4449)" class="st4 st5">{{.First.Seed}}</text>
		<rect id="XMLID_275_" x="137.3" y="601.4" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_117_" transform="matrix(1 0 0 1 140.02 608.9962)" class="st2 st4 st5">{{.Second.Seed}}</text>
		{{ end }}
	</g>
	<g id="XMLID_65_">
		<rect id="XMLID_106_" x="58" y="623.1" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_103_" x="58" y="638.6" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_96_" x="137" y="623.1" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_95_" x="58" y="638.3" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_94_" x="136.8" y="623.1" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_241_" x="58" y="626.8" class="st3" width="78.8" height="8.1"/>
		{{ with index .Matches 14 }}
		<text id="XMLID_92_" transform="matrix(1 0 0 1 60.3442 634.4449)" class="st2 st4 st5">{{.First.FirstName}} {{.First.LastName}}</text>
		<rect id="XMLID_236_" x="58" y="642.4" class="st3" width="78.8" height="8.1"/>
		<text id="XMLID_89_" transform="matrix(1 0 0 1 60.3442 649.9962)" class="st2 st4 st5">{{.Second.FirstName}} {{.Second.LastName}}</text>
		<rect id="XMLID_209_" x="137.3" y="626.8" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_78_" transform="matrix(1 0 0 1 140.02 634.4449)" class="st4 st5">{{.First.Seed}}</text>
		<rect id="XMLID_199_" x="137.3" y="642.4" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_67_" transform="matrix(1 0 0 1 140.02 649.9962)" class="st2 st4 st5">{{.Second.Seed}}</text>
		{{ end }}
	</g>
	<g id="XMLID_18_">
		<rect id="XMLID_63_" x="58" y="665.1" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_62_" x="58" y="680.6" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_61_" x="137" y="665.1" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_60_" x="58" y="680.3" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_53_" x="136.8" y="665.1" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_151_" x="58" y="668.8" class="st3" width="78.8" height="8.1"/>
		{{ with index .Matches 15 }}
		<text id="XMLID_47_" transform="matrix(1 0 0 1 60.3442 676.4449)" class="st2 st4 st5">{{.First.FirstName}} {{.First.LastName}}</text>
		<rect id="XMLID_141_" x="58" y="684.4" class="st3" width="78.8" height="8.1"/>
		<text id="XMLID_39_" transform="matrix(1 0 0 1 60.3442 691.9962)" class="st2 st4 st5">{{.Second.FirstName}} {{.Second.LastName}}</text>
		<rect id="XMLID_129_" x="137.3" y="668.8" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_31_" transform="matrix(1 0 0 1 140.02 676.4449)" class="st4 st5">{{.First.Seed}}</text>
		<rect id="XMLID_118_" x="137.3" y="684.4" class="st3" width="16.4" height="8.1"/>
		<text id="XMLID_22_" transform="matrix(1 0 0 1 140.02 691.9962)" class="st2 st4 st5">{{.Second.Seed}}</text>
		{{ end }}
	</g>
</g>
<g id="Layer_1_copy">
	<g id="XMLID_832_">
		<rect id="XMLID_849_" x="244.4" y="52.6" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_848_" x="244.4" y="68.1" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_847_" x="323.4" y="52.6" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_846_" x="244.4" y="67.9" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_845_" x="323.1" y="52.6" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_844_" x="244.4" y="56.3" class="st3" width="78.8" height="8.1"/>
	</g>
	<g id="XMLID_814_">
		<rect id="XMLID_831_" x="244.4" y="139.4" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_830_" x="244.4" y="155" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_829_" x="323.4" y="139.4" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_828_" x="244.4" y="154.7" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_827_" x="323.1" y="139.4" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_826_" x="244.4" y="143.2" class="st3" width="78.8" height="8.1"/>
	</g>
	<g id="XMLID_796_">
		<rect id="XMLID_813_" x="244.4" y="222.8" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_812_" x="244.4" y="238.3" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_811_" x="323.4" y="222.8" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_810_" x="244.4" y="238" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_809_" x="323.1" y="222.8" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_808_" x="244.4" y="226.5" class="st3" width="78.8" height="8.1"/>
	</g>
	<g id="XMLID_778_">
		<rect id="XMLID_795_" x="244.4" y="309.4" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_794_" x="244.4" y="325" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_793_" x="323.4" y="309.4" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_792_" x="244.4" y="324.7" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_791_" x="323.1" y="309.4" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_790_" x="244.4" y="313.2" class="st3" width="78.8" height="8.1"/>
	</g>
	<g id="XMLID_760_">
		<rect id="XMLID_777_" x="244.4" y="393.9" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_776_" x="244.4" y="409.5" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_775_" x="323.4" y="393.9" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_774_" x="244.4" y="409.2" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_773_" x="323.1" y="393.9" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_772_" x="244.4" y="397.7" class="st3" width="78.8" height="8.1"/>
	</g>
	<g id="XMLID_742_">
		<rect id="XMLID_759_" x="244.4" y="480.5" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_758_" x="244.4" y="496" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_757_" x="323.4" y="480.5" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_756_" x="244.4" y="495.8" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_755_" x="323.1" y="480.5" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_754_" x="244.4" y="484.2" class="st3" width="78.8" height="8.1"/>
	</g>
	<g id="XMLID_724_">
		<rect id="XMLID_741_" x="244" y="563.4" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_740_" x="244" y="579" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_739_" x="323" y="563.4" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_738_" x="244" y="578.7" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_737_" x="322.8" y="563.4" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_736_" x="244" y="567.2" class="st3" width="78.8" height="8.1"/>
	</g>
	<g id="XMLID_706_">
		<rect id="XMLID_723_" x="244.4" y="650.9" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_722_" x="244.4" y="666.5" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_721_" x="323.4" y="650.9" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_720_" x="244.4" y="666.2" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_719_" x="323.1" y="650.9" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_718_" x="244.4" y="654.7" class="st3" width="78.8" height="8.1"/>
	</g>
</g>
<g id="Layer_1_copy_2">
	<g id="XMLID_273_">
		<rect id="XMLID_308_" x="454.6" y="95.9" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_307_" x="454.6" y="111.5" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_306_" x="533.7" y="95.9" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_305_" x="454.6" y="111.2" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_304_" x="533.4" y="95.9" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_298_" x="454.6" y="99.7" class="st3" width="78.8" height="8.1"/>
	</g>
	<g id="XMLID_201_">
		<rect id="XMLID_272_" x="454.6" y="265.9" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_271_" x="454.6" y="281.5" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_270_" x="533.7" y="265.9" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_269_" x="454.6" y="281.2" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_268_" x="533.4" y="265.9" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_262_" x="454.6" y="269.7" class="st3" width="78.8" height="8.1"/>
	</g>
	<g id="XMLID_123_">
		<rect id="XMLID_197_" x="454.6" y="438.4" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_194_" x="454.6" y="454" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_176_" x="533.7" y="438.4" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_170_" x="454.6" y="453.7" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_169_" x="533.4" y="438.4" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_166_" x="454.6" y="442.2" class="st3" width="78.8" height="8.1"/>
	</g>
	<g id="XMLID_10_">
		<rect id="XMLID_122_" x="454.6" y="607.9" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_119_" x="454.6" y="623.5" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_116_" x="533.7" y="607.9" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_100_" x="454.6" y="623.2" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_93_" x="533.4" y="607.9" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_90_" x="454.6" y="611.7" class="st3" width="78.8" height="8.1"/>
	</g>
</g>
<g id="Layer_1_copy_3">
	<g id="XMLID_616_">
		<rect id="XMLID_633_" x="664.7" y="182.2" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_632_" x="664.7" y="197.7" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_631_" x="743.8" y="182.2" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_630_" x="664.7" y="197.4" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_629_" x="743.5" y="182.2" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_628_" x="664.7" y="185.9" class="st3" width="78.8" height="8.1"/>
	</g>
	<g id="XMLID_598_">
		<rect id="XMLID_615_" x="664.7" y="522.3" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_614_" x="664.7" y="537.8" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_613_" x="743.8" y="522.3" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_612_" x="664.7" y="537.5" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_611_" x="743.5" y="522.3" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_610_" x="664.7" y="526" class="st3" width="78.8" height="8.1"/>
	</g>
</g>
<g id="Layer_1_copy_4">
	<g id="XMLID_577_">
		<rect id="XMLID_597_" x="851.3" y="344.4" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_596_" x="851.3" y="360" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_595_" x="930.3" y="344.4" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_594_" x="851.3" y="359.7" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_593_" x="930" y="344.4" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_592_" x="851.3" y="348.2" class="st3" width="78.8" height="8.1"/>
	</g>
	<g id="XMLID_332_">
		<rect id="XMLID_353_" x="664.7" y="522.3" class="st0" width="95.8" height="15.6"/>
		<rect id="XMLID_352_" x="664.7" y="537.8" class="st1" width="95.8" height="15.6"/>
		<rect id="XMLID_351_" x="743.8" y="522.3" class="st2" width="16.7" height="15.6"/>
		<rect id="XMLID_350_" x="664.7" y="537.5" class="st2" width="95.8" height="0.6"/>
		<rect id="XMLID_349_" x="743.5" y="522.3" class="st2" width="0.6" height="31.1"/>
		<rect id="XMLID_348_" x="664.7" y="526" class="st3" width="78.8" height="8.1"/>
	</g>
</g>
<g id="Layer_7">
	<g id="Layer_6">
		<path id="XMLID_309_" class="st6" d="M153.8,46"/>
		<g id="XMLID_316_">
			<g id="XMLID_314_">
				<line id="XMLID_310_" class="st7" x1="154" y1="45.5" x2="197" y2="45.5"/>
				<line id="XMLID_312_" class="st7" x1="196.5" y1="46" x2="196.5" y2="68"/>
				<line id="XMLID_313_" class="st7" x1="196" y1="68.5" x2="244" y2="68.5"/>
			</g>
			<g id="XMLID_317_">
				<line id="XMLID_321_" class="st7" x1="154" y1="91.5" x2="197" y2="91.5"/>
				<line id="XMLID_320_" class="st7" x1="196.5" y1="91" x2="196.5" y2="69"/>
				<line id="XMLID_318_" class="st7" x1="196" y1="68.5" x2="244" y2="68.5"/>
			</g>
		</g>
	</g>
	<g id="Layer_6_copy">
		<path id="XMLID_354_" class="st6" d="M153.8,46"/>
		<g id="XMLID_322_">
			<g id="XMLID_328_">
				<line id="XMLID_331_" class="st7" x1="154" y1="132.5" x2="197" y2="132.5"/>
				<line id="XMLID_330_" class="st7" x1="196.5" y1="133" x2="196.5" y2="155"/>
				<line id="XMLID_329_" class="st7" x1="196" y1="155.5" x2="244" y2="155.5"/>
			</g>
			<g id="XMLID_323_">
				<line id="XMLID_327_" class="st7" x1="154" y1="178.5" x2="197" y2="178.5"/>
				<line id="XMLID_326_" class="st7" x1="196.5" y1="178" x2="196.5" y2="156"/>
				<line id="XMLID_325_" class="st7" x1="196" y1="155.5" x2="244" y2="155.5"/>
			</g>
		</g>
	</g>
	<g id="Layer_6_copy_3">
		<path id="XMLID_378_" class="st6" d="M153.8,46"/>
		<g id="XMLID_368_">
			<g id="XMLID_374_">
				<line id="XMLID_377_" class="st7" x1="154" y1="215.5" x2="197" y2="215.5"/>
				<line id="XMLID_376_" class="st7" x1="196.5" y1="216" x2="196.5" y2="238"/>
				<line id="XMLID_375_" class="st7" x1="196" y1="238.5" x2="244" y2="238.5"/>
			</g>
			<g id="XMLID_370_">
				<line id="XMLID_373_" class="st7" x1="154" y1="261.5" x2="197" y2="261.5"/>
				<line id="XMLID_372_" class="st7" x1="196.5" y1="261" x2="196.5" y2="239"/>
				<line id="XMLID_371_" class="st7" x1="196" y1="238.5" x2="244" y2="238.5"/>
			</g>
		</g>
	</g>
	<g id="Layer_6_copy_2">
		<path id="XMLID_367_" class="st6" d="M153.8,46"/>
		<g id="XMLID_380_">
			<g id="XMLID_387_">
				<line id="XMLID_391_" class="st7" x1="154" y1="302.5" x2="197" y2="302.5"/>
				<line id="XMLID_390_" class="st7" x1="196.5" y1="303" x2="196.5" y2="325"/>
				<line id="XMLID_389_" class="st7" x1="196" y1="325.5" x2="244" y2="325.5"/>
			</g>
			<g id="XMLID_382_">
				<line id="XMLID_386_" class="st7" x1="154" y1="348.5" x2="197" y2="348.5"/>
				<line id="XMLID_385_" class="st7" x1="196.5" y1="348" x2="196.5" y2="326"/>
				<line id="XMLID_383_" class="st7" x1="196" y1="325.5" x2="244" y2="325.5"/>
			</g>
		</g>
	</g>
	<g id="Layer_6_copy_7">
		<path id="XMLID_426_" class="st6" d="M153.8,46"/>
		<g id="XMLID_417_">
			<g id="XMLID_422_">
				<line id="XMLID_425_" class="st7" x1="154" y1="386.5" x2="197" y2="386.5"/>
				<line id="XMLID_424_" class="st7" x1="196.5" y1="387" x2="196.5" y2="409"/>
				<line id="XMLID_423_" class="st7" x1="196" y1="409.5" x2="244" y2="409.5"/>
			</g>
			<g id="XMLID_418_">
				<line id="XMLID_421_" class="st7" x1="154" y1="432.5" x2="197" y2="432.5"/>
				<line id="XMLID_420_" class="st7" x1="196.5" y1="432" x2="196.5" y2="410"/>
				<line id="XMLID_419_" class="st7" x1="196" y1="409.5" x2="244" y2="409.5"/>
			</g>
		</g>
	</g>
	<g id="Layer_6_copy_6">
		<path id="XMLID_415_" class="st6" d="M153.8,46"/>
		<g id="XMLID_450_">
			<g id="XMLID_458_">
				<line id="XMLID_461_" class="st7" x1="154" y1="473.5" x2="197" y2="473.5"/>
				<line id="XMLID_460_" class="st7" x1="196.5" y1="474" x2="196.5" y2="496"/>
				<line id="XMLID_459_" class="st7" x1="196" y1="496.5" x2="244" y2="496.5"/>
			</g>
			<g id="XMLID_452_">
				<line id="XMLID_456_" class="st7" x1="154" y1="519.5" x2="197" y2="519.5"/>
				<line id="XMLID_455_" class="st7" x1="196.5" y1="519" x2="196.5" y2="497"/>
				<line id="XMLID_453_" class="st7" x1="196" y1="496.5" x2="244" y2="496.5"/>
			</g>
		</g>
	</g>
	<g id="Layer_6_copy_5">
		<path id="XMLID_402_" class="st6" d="M153.8,46"/>
		<g id="XMLID_392_">
			<g id="XMLID_398_">
				<line id="XMLID_401_" class="st7" x1="154" y1="556.5" x2="197" y2="556.5"/>
				<line id="XMLID_400_" class="st7" x1="196.5" y1="557" x2="196.5" y2="579"/>
				<line id="XMLID_399_" class="st7" x1="196" y1="579.5" x2="244" y2="579.5"/>
			</g>
			<g id="XMLID_394_">
				<line id="XMLID_397_" class="st7" x1="154" y1="602.5" x2="197" y2="602.5"/>
				<line id="XMLID_396_" class="st7" x1="196.5" y1="602" x2="196.5" y2="580"/>
				<line id="XMLID_395_" class="st7" x1="196" y1="579.5" x2="244" y2="579.5"/>
			</g>
		</g>
	</g>
	<g id="Layer_6_copy_4">
		<path id="XMLID_379_" class="st6" d="M153.8,46"/>
		<g id="XMLID_429_">
			<g id="XMLID_436_">
				<line id="XMLID_440_" class="st7" x1="154" y1="643.5" x2="197" y2="643.5"/>
				<line id="XMLID_439_" class="st7" x1="196.5" y1="644" x2="196.5" y2="666"/>
				<line id="XMLID_437_" class="st7" x1="196" y1="666.5" x2="244" y2="666.5"/>
			</g>
			<g id="XMLID_430_">
				<line id="XMLID_434_" class="st7" x1="154" y1="689.5" x2="197" y2="689.5"/>
				<line id="XMLID_433_" class="st7" x1="196.5" y1="689" x2="196.5" y2="667"/>
				<line id="XMLID_431_" class="st7" x1="196" y1="666.5" x2="244" y2="666.5"/>
			</g>
		</g>
	</g>
</g>
<g id="Layer_16">
	<g id="Layer_6_copy_8">
		<path id="XMLID_404_" class="st6" d="M153.8,46"/>
		<g id="XMLID_360_">
			<line id="XMLID_355_" class="st8" x1="340" y1="155" x2="399" y2="155"/>
			<line id="XMLID_356_" class="st8" x1="340" y1="68" x2="399" y2="68"/>
			<line id="XMLID_357_" class="st8" x1="399" y1="68" x2="399" y2="155"/>
			<line id="XMLID_359_" class="st8" x1="399" y1="112" x2="455" y2="112"/>
		</g>
	</g>
	<g id="Layer_6_copy_9">
		<path id="XMLID_406_" class="st6" d="M153.8,46"/>
		<g id="XMLID_361_">
			<line id="XMLID_405_" class="st8" x1="340" y1="325" x2="399" y2="325"/>
			<line id="XMLID_366_" class="st8" x1="340" y1="238" x2="399" y2="238"/>
			<line id="XMLID_364_" class="st8" x1="399" y1="238" x2="399" y2="325"/>
			<line id="XMLID_363_" class="st8" x1="399" y1="282" x2="455" y2="282"/>
		</g>
	</g>
	<g id="Layer_6_copy_11">
		<path id="XMLID_445_" class="st6" d="M153.8,46"/>
		<g id="XMLID_428_">
			<line id="XMLID_444_" class="st8" x1="340" y1="497" x2="399" y2="497"/>
			<line id="XMLID_443_" class="st8" x1="340" y1="410" x2="399" y2="410"/>
			<line id="XMLID_442_" class="st8" x1="399" y1="410" x2="399" y2="497"/>
			<line id="XMLID_441_" class="st8" x1="399" y1="454" x2="455" y2="454"/>
		</g>
	</g>
	<g id="Layer_6_copy_10">
		<path id="XMLID_414_" class="st6" d="M153.8,46"/>
		<g id="XMLID_447_">
			<line id="XMLID_464_" class="st8" x1="340" y1="667" x2="399" y2="667"/>
			<line id="XMLID_463_" class="st8" x1="340" y1="580" x2="399" y2="580"/>
			<line id="XMLID_449_" class="st8" x1="399" y1="580" x2="399" y2="667"/>
			<line id="XMLID_448_" class="st8" x1="399" y1="624" x2="455" y2="624"/>
		</g>
	</g>
</g>
<g id="Layer_22">
	<g id="Layer_20">
		<g id="XMLID_413_">
			<line id="XMLID_407_" class="st9" x1="550" y1="111.5" x2="604" y2="111.5"/>
			<line id="XMLID_409_" class="st9" x1="550" y1="281.5" x2="604" y2="281.5"/>
			<line id="XMLID_410_" class="st9" x1="603.5" y1="112" x2="603.5" y2="282"/>
		</g>
		<line id="XMLID_411_" class="st9" x1="665" y1="197.5" x2="603" y2="197.5"/>
	</g>
	<g id="Layer_20_copy">
		<g id="XMLID_465_">
			<line id="XMLID_468_" class="st9" x1="550" y1="452.5" x2="604" y2="452.5"/>
			<line id="XMLID_467_" class="st9" x1="550" y1="622.5" x2="604" y2="622.5"/>
			<line id="XMLID_466_" class="st9" x1="603.5" y1="453" x2="603.5" y2="623"/>
		</g>
		<line id="XMLID_446_" class="st9" x1="665" y1="538.5" x2="603" y2="538.5"/>
	</g>
</g>
<g id="Layer_23">
	<line id="XMLID_469_" class="st10" x1="760" y1="198" x2="808" y2="198"/>
	<line id="XMLID_470_" class="st10" x1="760" y1="542" x2="808" y2="542"/>
	<line id="XMLID_471_" class="st10" x1="808" y1="198" x2="808" y2="542"/>
	<line id="XMLID_472_" class="st10" x1="808" y1="361" x2="851" y2="361"/>
</g>
</svg>`
