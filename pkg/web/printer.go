package printer

import (
	"encoding/json"
	"html/template"
	"log"
	"net/http"
	"strconv"
)

// PrintLadder implements Printer interface. It looks and
// prints leaf nodes . In this case it parses and executes
// svg based template, and returns it.
func (s *Server) PrintLadder(ps Players) string {

	fn := func(w http.ResponseWriter, r *http.Request) {
		params := r.URL.Query()

		t, err := GenerateTree(ps)
		if err != nil {
			log.Fatal(err)
		}

		var ma []Match
		t.Traverse(t.Root, printNode(&ma), 0)

		a := struct {
			Matches []Match
		}{
			Matches: ma,
		}

		if params["name"] != nil && params["round"] != nil {
			round, err := strconv.Atoi(params["round"][0])
			if err != nil {
				http.Error(w, "Something went wrong", http.StatusInternalServerError)
			}
			t.Search(params["name"][0], round, w)
		}
		w.Header().Set("Content-Type", "image/svg+xml")
		temp := template.New("svg")
		temp, err = temp.Parse(web.Svg)
		if err != nil {
			http.Error(w, "Something went wrong", http.StatusInternalServerError)
		}
		err = temp.Execute(w, a)
	}

	return fn
}

// PrintTable prints a list of players that represents predictions
// resolved possible opponents for a player. This doesn't acctually
// print anything, it returns formatted JSON, and client-side JS will
// format it into a table. This is done because this is not worth
// implementing with go templates
func (s *Server) PrintTable(ps Players) ([]byte, error) {
	var b []byte
	b, err := json.Marshal(ps)
	if err != nil {
		return b, err
	}

	return b, nil
}

func printNode(m *[]Match) func(n *Node, depth int) {
	return func(n *Node, depth int) {
		if depth == 4 {
			// log.Printf("Match: { %d vs %d }\t Depth:%d", n.Value.First.Seed, n.Value.Second.Seed, depth)
			*m = append(*m, n.Value)
		}
	}
}
