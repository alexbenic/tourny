package web

import (
	"log"
	"net/http"
	"time"
)

const (
	port    = 8000
	address = "localhost"
)

// Server ...
type Server struct {
	*Conf
	App *App
}

// Conf is a way to configure your server
type Conf struct {
	Port    uint
	Address string
	App     *App
}

// New initiates Server
func New(c Conf) *Server {
	s := &Server{Conf: c}

	s.handler = s.handler()
}

func (s *Server) handler() *http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s\t %s\n", r.Method, r.URL.String())

		if r.URL.String() == "/generate" && r.Method == "GET" {
			s.handleGenerate(w, r)
		} else if r.URL.String() == "/search" && r.Method == "GET" {
			s.handleSearch(w, r)
		} else {
			http.NotFound(w, r)
		}
	}

	return http.HandlerFunc(fn)
}

func (s *Server) handleGenerate(w http.ResponseWriter, r *http.Request) {
	err := s.App.Generate()
	if err != nil {
		log.Printf("err = %+v\n", err)
		return
	}

	l, err := s.PrintLadder()
	if err != nil {
		log.Printf("err = %+v\n", err)
		return
	}

	w.Header().Set("Content-Type", "image/svg+xml")
	w.Write([]byte(l))
}

func (s *Server) handleSearch(w http.ResponseWriter, r *http.Request) {

}

// Serve starts the server
func (s *Server) Serve() {
	srv := http.Server{
		Handler:      s.handler,
		Addr:         s.address,
		Port:         s.port,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}
