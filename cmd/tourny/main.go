package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	"gitlab.com/alexbenic/tourny/pkg/app"
	"gitlab.com/alexbenic/tourny/pkg/web"
)

var (
	port    = flag.Int("p", 8000, "port")
	address = flag.String("s", "localhost", "address")
)

func init() {
	// Seed global rand generator
	rand.Seed(time.Now().UTC().UnixNano())

}

const ()

func main() {
	flag.usage = help
	flag.Parse()

	if len(os.Args) != 1 {
		help()
		os.Exit(2)
	}

	srv := web.New(&web.Conf{
		Port:    uint(*port),
		Address: adress,
		App:     app.New(flags.Arg(0)),
	})

	log.Printf("Running on http://%s:%d/", *address, *port)
	srv.Serve()
}

func help() {
	fmt.Fprintln(os.Stderr, `Usage:
	tourny <path_to_json>
		-s	bind address (default: localhost)
		-p	port (default: 8000)
	`)
}
