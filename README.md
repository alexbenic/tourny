# tourny
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/alexbenic/tourny)](https://goreportcard.com/report/gitlab.com/alexbenic/tourny)

![tourny](./art/knight.png)

Generate tournament skeleton from a JSON file


## Install:
To install tourny, simply:
```
  $ go get gitlab.com/alexbenic/tourny/cmd/tourny
```

## Usage :
```
  tourny <path>
    -s address (default: localhost)
    -p port (default: 8000)
```

## Warning :
Due to heavy refactoring code might break.

## License
MIT
