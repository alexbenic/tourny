.PHONY: build package build-linux build-osx build-windows todo


DIST_ROOT=dist

GO = go

dist: | package

build-linux:
	@echo Building Linux $(ARCH)
	env GOOS=linux GOARCH=$(ARCH) $(GO) build -o tourny-linux ./cmd/tourny

build-osx:
	@echo Building OSX $(ARCH)
	env GOOS=darwin GOARCH=$(ARCH) $(GO) build -o tourny-osx ./cmd/tourny

build-windows:
	@echo Building Windows $(ARCH)
	env GOOS=windows GOARCH=$(ARCH) $(GO) build -o tourny-win.exe ./cmd/tourny

all: build-linux build-windows build-osx

package:
	@echo Packaging

	@# Remove old files
	rm -Rf $(DIST_ROOT)

	@# Create needed directories
	mkdir -p $(DIST_ROOT)/bin
	mkdir -p $(DIST_ROOT)/code

	@# Resources direcories
	cp -RL cmd $(DIST_ROOT)/code
	cp -RL pkg $(DIST_ROOT)/code

	@# Readme && docs
	cp README.md $(DIST_ROOT)
	cp LICENSE $(DIST_ROOT)

	@# Win
	mv tourny-win.exe $(DIST_ROOT)/bin/tourny.exe

	@# OSX
	mv tourny-osx $(DIST_ROOT)/bin/tourny-osx

	@# JSON file
	cp players.json $(DIST_ROOT)/bin/players.json

	@# Linux
	mv tourny-linux $(DIST_ROOT)/bin/tourny-linux
	@# Linux#Compress
	tar -cvzf tourny.tar.gz dist


clean:
	@echo Cleaning
	rm -Rf $(DIST_ROOT)

todo:
	@ag --ignore Makefile --ignore-dir server/vendor --ignore-dir client/node_modules TODO
	@ag --ignore Makefile --ignore-dir server/vendor --ignore-dir client/node_modules FIX
	@ag --ignore Makefile --ignore-dir server/vendor --ignore-dir client/node_modules "FIX"
